# coding: utf-8
require 'rubygems'
require 'sinatra'
require 'erb'
require 'sqlite3'
require File.expand_path(File.dirname(__FILE__))  + '/model/registration'

get '/' do
  erb :index
end

get '/contact' do
  erb :contact
end

post '/contact' do
  if params["name"] == "" && params["email"]                                
    @msg =  "<div class='alert alert-error'>Por Favor, Preencha Corretamente os Campos</div>"                      
    erb :contact
  else
    registration = Registration.new(params)                               
    registration.save                                             
    @msg = "<div class='alert alert-info'>Obrigado, Em Breve Retornaremos o Contato.</div>"
    erb :contact
  end
end

get '/services' do
  erb :services
end

get '/teste' do
  erb :teste
end



