# coding: utf-8
class Connection
  attr_accessor :db
  def initialize
    @db = SQLite3::Database.new( "contact.db" )
  end

  def executar(sql)
    @db.execute sql
  end
end
